#!/bin/bash

pacman -Syu --needed --noconfirm
pacman -S --needed --noconfirm wget make gcc libcap openssh sudo \
                               inetutils base-devel libnsl man vi \
                               vim python3 git
pacman -S --needed --noconfirm arduino arduino-docs arduino-cli
